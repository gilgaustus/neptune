import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AcknowledgementsPageRoutingModule } from './acknowledgements-routing.module';

import { AcknowledgementsPage } from './acknowledgements.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AcknowledgementsPageRoutingModule
  ],
  declarations: [AcknowledgementsPage]
})
export class AcknowledgementsPageModule {}
