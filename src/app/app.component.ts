import { Component, HostListener, OnInit } from "@angular/core";

import { Platform, PopoverController } from "@ionic/angular";
import { SplashScreen } from "@ionic-native/splash-screen/ngx";
import { StatusBar } from "@ionic-native/status-bar/ngx";
import { ScreensizeService } from "./util/screensize.service";
import { PopoverSettingsComponent } from "./util/popoversettings/popoversettings.component";

@Component({
  selector: "app-root",
  templateUrl: "app.component.html",
  styleUrls: ["app.component.scss"],
})
export class AppComponent implements OnInit {
  public selectedIndex = 0;
  public labels = ["TBD"];
  isDesktop: boolean;

  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private screensizeService: ScreensizeService,
    private popoverController: PopoverController
  ) {
    this.initializeApp();
    this.screensizeService.isDesktopView().subscribe(isDesktop => {
      if (this.isDesktop && !isDesktop) {
        // Reload because our routing is out of place
        window.location.reload();
      }
 
      this.isDesktop = isDesktop;
    });
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
      this.screensizeService.onResize(this.platform.width(), this.platform.height());
    });
  }

  ngOnInit() {
    const path = window.location.pathname;
    // if (path !== undefined) {
    //   this.selectedIndex = this.appPages.findIndex(
    //     (page) => page.url.toLowerCase() === path.toLowerCase()
    //   );
    // }
  }

  async settingsPopover(ev: any) {
    const popover = await this.popoverController.create({
      component: PopoverSettingsComponent,
      event: ev,
      cssClass: 'menu-popover',
      translucent: true,
      mode: 'md',
    });

    return await popover.present();
  }

  @HostListener('window:resize', ['$event'])
  private onResize(event) {
    this.screensizeService.onResize(event.target.innerWidth, event.target.innerHeight);
  }
}
