import { Component, OnInit } from "@angular/core";
import { PopoverController } from "@ionic/angular";

@Component({
  selector: "app-popoversettings",
  templateUrl: "./popoversettings.component.html",
  styleUrls: ["./popoversettings.component.scss"],
})
export class PopoverSettingsComponent implements OnInit {
  public selectedIndex = 0;
  public appPages = [
    {
      title: "Home",
      url: "/home",
      icon: "heart",
    },
    {
      title: "Event Details",
      url: "/home/details",
      icon: "checkbox",
      isSubMenu: false,
    },
    {
      title: "Venue",
      url: "/home/venue",
      icon: "wedding-rings",
      isSubMenu: false,
    },
    {
      title: "RSVP",
      url: "/home/rsvp",
      icon: "checkbox",
      isSubMenu: false,
    },
    {
      title: "Registry",
      url: "/home/registry",
      icon: "gift",
      isSubMenu: false,
    },
    {
      title: "Accomodations",
      url: "/home/accomodations",
      icon: "airplane",
      isSubMenu: false,
    },
    {
      title: "COVID-19",
      url: "/home/covid",
      icon: "airplane",
      isSubMenu: false,
    },
    {
      title: "Acknowledgements",
      url: "/acknowledgements",
      icon: "trophy",
      isSubMenu: false,
    },
  ];

  site;

  constructor(private popoverController: PopoverController) {}

  ngOnInit() {
  }

  dismiss() {
    this.popoverController.dismiss();
  }

}
