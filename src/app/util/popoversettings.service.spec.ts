import { TestBed } from '@angular/core/testing';

import { PopoversettingsService } from './popoversettings.service';

describe('PopoversettingsService', () => {
  let service: PopoversettingsService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PopoversettingsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
