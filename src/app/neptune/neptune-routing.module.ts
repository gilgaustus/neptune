import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";

import { NeptunePage } from "./neptune.page";

const routes: Routes = [
  // {
  //   path: '',
  //   component: NeptunePage
  // },
  {
    path: "",
    loadChildren: () =>
      import("./wedding/wedding.module").then((m) => m.WeddingPageModule),
  },
  {
    path: "wedding",
    loadChildren: () =>
      import("./wedding/wedding.module").then((m) => m.WeddingPageModule),
  },
  {
    path: "memories/:memoryId",
    loadChildren: () =>
      import("./memories/memories.module").then((m) => m.MemoriesPageModule),
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class NeptunePageRoutingModule {}
