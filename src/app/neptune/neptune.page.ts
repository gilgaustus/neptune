import { CssSelector } from "@angular/compiler";
import { Component, OnInit } from "@angular/core";
import { Platform } from "@ionic/angular";
import { NEPTUNE_MODELS } from "./models/neptune-models";

@Component({
  selector: "app-neptune",
  templateUrl: "./neptune.page.html",
  styles: [``],
  styleUrls: ["./neptune.page.scss"],
})
export class NeptunePage implements OnInit {
  ourImages = NEPTUNE_MODELS.getOurImages;
  ourMemories = NEPTUNE_MODELS.getOurMemories;
  bImage = `ion-content {
    --background: #fff url(toReplace) no-repeat center center / cover;
}`;

  slideOptsOne = {
    initialSlide: 0,
    slidesPerView: 4,
    autoplay: true,
  };

  constructor(private platform: Platform) {}

  ngOnInit() {}

  getDeviceDetails() {
    if (this.platform.isPortrait && this.platform.is("mobile")) {
      this.bImage.replace(
        "toReplace",
        "../../assets/images/main_background_portrait.png"
      );
    } else if (this.platform.isLandscape && this.platform.is("mobile")) {
      this.bImage.replace(
        "toReplace",
        "../../assets/images/main_background_landscape.png"
      );
    } else if (this.platform.isPortrait && this.platform.is("tablet")) {
      this.bImage.replace(
        "toReplace",
        "../../assets/images/main_background_16x20.png"
      );
    } else {
      this.bImage.replace(
        "toReplace",
        "../../assets/images/main_background_16x20.png"
      );
    }
  }
}
