import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-memories',
  templateUrl: './memories.page.html',
  styleUrls: ['./memories.page.scss'],
})
export class MemoriesPage implements OnInit {

  constructor(private route: ActivatedRoute) { }

  travelId = '';
  ngOnInit() {
    this.route.paramMap.subscribe(params => {
      this.travelId = params.get('memoryId');

   });
  }

}
