import { NeptuneTravel } from "./neptune-travel";

export class NEPTUNE_MODELS {
  constructor() {}
  private static ourImages = [
    "../../assets/images/Us/1.jpg",
    "../../assets/images/Us/2.jpg",
    "../../assets/images/Us/3.jpg",
    "../../assets/images/Us/4.jpg",
    "../../assets/images/Us/5.jpg",
    "../../assets/images/Us/6.jpg",
    "../../assets/images/Us/6-.jpg",
    "../../assets/images/Us/7.jpg",
    "../../assets/images/Us/8.jpg",
    "../../assets/images/Us/9.jpg",
    "../../assets/images/Us/10.jpg",
    "../../assets/images/Us/11.jpg",
    "../../assets/images/Us/12.jpg",
    "../../assets/images/Us/13.jpg",
    "../../assets/images/Us/14.png",
    "../../assets/images/Us/15.jpg",
    "../../assets/images/Us/16.jpg",
    "../../assets/images/Us/17.jpg",
    "../../assets/images/Us/18.jpg",
    "../../assets/images/Us/19.jpeg",
    "../../assets/images/Us/20.jpg",
    "../../assets/images/Us/21.jpg",
    "../../assets/images/Us/22.jpg",
  ];
  private static lodgeBromley = [
    "../../assets/images/wedding/lodge-at-bromley/1.jpg",
    "../../assets/images/wedding/lodge-at-bromley/2.jpg",
    "../../assets/images/wedding/lodge-at-bromley/3.jpg",
    "../../assets/images/wedding/lodge-at-bromley/4.jpg",
    "../../assets/images/wedding/lodge-at-bromley/5.jpg",
    "../../assets/images/wedding/lodge-at-bromley/6.jpg",
  ];

  private static ourTravels: NeptuneTravel[] = [
    { name: "Puerto Rico 2018", id: "pr2018" },
    { name: "Ireland", id: "ireland" },
    { name: "Disney 2019", id: "disney2019" },
    { name: "Paul & Jess's Wedding", id: "pauljess" },
    { name: "Vermont 2019", id: "vermont2019" },
    { name: "Puerto Rico 2019", id: "pr2019" },
    { name: "Ireland", id: "ireland" },
  ];

  public static get getOurImages() {
    return this.ourImages;
  }

  public static get getOurMemories() {
    return this.ourTravels;
  }

  public static get getLodge() {
    return this.lodgeBromley;
  }

  public getMemories(memoryId: string) {}
}
