import {
  AfterViewInit,
  Component,
  ElementRef,
  HostListener,
  OnInit,
  ViewChild,
} from "@angular/core";

@Component({
  selector: "app-wedding",
  templateUrl: "./wedding.page.html",
  styleUrls: ["./wedding.page.scss"],
})
export class WeddingPage implements OnInit {

  screenHeight: number;
  iframeWidth;

  // @ViewChild("card", { read: ElementRef }) card: ElementRef;

  constructor() {}

  ngOnInit() {}

  // ngAfterViewInit(): void {
  //   this.iframeWidth = "80%";
  //   console.log(this.card.nativeElement.offsetWidth);
  // }

  // @HostListener("window:resize", ["$event"])
  // onResize(event) {
  //   this.iframeWidth = "80%";
  // }
}
