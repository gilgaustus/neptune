import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { WeddingPage } from './wedding.page';

const routes: Routes = [
  {
    path: '',
    component: WeddingPage
  },
  {
    path: 'registry',
    loadChildren: () => import('./registry/registry.module').then( m => m.RegistryPageModule)
  },
  {
    path: 'accomodations',
    loadChildren: () => import('./accomodations/accomodations.module').then( m => m.AccomodationsPageModule)
  },
  {
    path: "rsvp",
    loadChildren: () =>
      import("./rsvp/rsvp.module").then((m) => m.RsvpPageModule),
  },
  {
    path: 'venue',
    loadChildren: () => import('./venue/venue.module').then( m => m.VenuePageModule)
  },
  {
    path: 'covid',
    loadChildren: () => import('./covid/covid.module').then( m => m.CovidPageModule)
  },
  {
    path: 'details',
    loadChildren: () => import('./details/details.module').then( m => m.DetailsPageModule)
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class WeddingPageRoutingModule {}
