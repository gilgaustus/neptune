import {
  AfterViewInit,
  Component,
  ElementRef,
  OnInit,
  SecurityContext,
  ViewChild,
} from "@angular/core";
import { DomSanitizer, SafeResourceUrl } from "@angular/platform-browser";

@Component({
  selector: "app-registry",
  templateUrl: "./registry.page.html",
  styleUrls: ["./registry.page.scss"],
})
export class RegistryPage implements OnInit, AfterViewInit {
  @ViewChild("iframe") iframe: ElementRef;

  url: string = "http://www.zola.com/registry/gilbertoandmadison";
  urlSafe: SafeResourceUrl;

  constructor(public sanitizer: DomSanitizer) {}

  ngOnInit() {
    this.urlSafe = this.sanitizer.bypassSecurityTrustUrl(
      "http://www.etsy.com/registry/Njg1NzEwNTZ8MjkyNDM3Njc/"
    );
  }

  ngAfterViewInit(): void {}
}
