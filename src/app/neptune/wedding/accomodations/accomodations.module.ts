import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AccomodationsPageRoutingModule } from './accomodations-routing.module';

import { AccomodationsPage } from './accomodations.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AccomodationsPageRoutingModule
  ],
  declarations: [AccomodationsPage]
})
export class AccomodationsPageModule {}
