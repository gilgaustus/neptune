import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { WeddingPage } from './wedding.page';

describe('WeddingPage', () => {
  let component: WeddingPage;
  let fixture: ComponentFixture<WeddingPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WeddingPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(WeddingPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
