import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { RSVP } from './rsvp';

@Injectable({
  providedIn: 'root'
})
export class RsvpService {

  constructor(private http: HttpClient) { }

  sendRsvp(rsvp: RSVP): Observable<any> {
    // const headers = new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8');
    return this.http.post('http://localhost:8100/rsvp', rsvp);
  }

  getRsvp(name: string): Observable<any> {
    // const headers = new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8');
    return this.http.get('http://localhost:8100/rsvp/' + name);
  }

  getRsvpList(): Observable<any> {
    // const headers = new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8');
    return this.http.get('http://localhost:8100/rsvp');
  }
}
