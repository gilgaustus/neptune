import { Component, HostListener, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { AlertController, Platform } from "@ionic/angular";
import { ScreensizeService } from "src/app/util/screensize.service";
import { RSVP } from "./rsvp";
import { RsvpService } from "./rsvp.service";

@Component({
  selector: "app-rsvp",
  templateUrl: "./rsvp.page.html",
  styleUrls: ["./rsvp.page.scss"],
})
export class RsvpPage implements OnInit {
  isDesktop: boolean;
  mealOptions = ["Meat", "Vegetarian", "Vegan"];
  rsvp = new RSVP();
  name = "";

  constructor(
    private screensizeService: ScreensizeService,
    private platform: Platform,
    private rsvpService: RsvpService,
    private alertController: AlertController,
    private router: Router
  ) {
    this.screensizeService.isDesktopView().subscribe((isDesktop) => {
      if (this.isDesktop && !isDesktop) {
        // Reload because our routing is out of place
        window.location.reload();
      }

      this.isDesktop = isDesktop;
    });
  }

  ngOnInit() {
    this.screensizeService.onResize(
      this.platform.width(),
      this.platform.height()
    );
  }

  getRsvp() {
    if (this.name == 'GetList') {
      window.open('http://localhost:8100/rsvp', "_blank");
    } else {
      this.rsvpService.getRsvp(this.name).subscribe(
        (res) => {
          this.rsvp = res;
        },
        (err) => {
          console.log(err);
        }
      );
    }
  }

  onSubmit() {
    this.rsvpService.sendRsvp(this.rsvp).subscribe(
      (res) => {
        console.log(res);
        this.presentAlert();
      },
      (err) => {
        console.log(err);
      }
    );
  }

  async presentAlert() {
    const alert = await this.alertController.create({
      header:
        "RSVP successfully received! Please feel free to browse our website.",
      buttons: [
        {
          text: "Registry",
          role: "rsvp",
          handler: () => {
            this.router.navigate(["/home/registry"]);
          },
        },
        {
          text: "COVID-19",
          role: "rsvp",
          handler: () => {
            this.router.navigate(["/home/covid"]);
          },
        },
      ],
    });

    await alert.present();

    const { role } = await alert.onDidDismiss();
  }

  @HostListener("window:resize", ["$event"])
  private onResize(event) {
    this.screensizeService.onResize(
      event.target.innerWidth,
      event.target.innerHeight
    );
  }
}
