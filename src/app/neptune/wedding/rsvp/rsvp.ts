export class RSVP {
    id: number;
    name: "";
    nameSecond: "";
    email: "";
    plusOneEnabled: true;
    plusOne: undefined;
    plusOneName: "";
    toAcceptOrNotToAccept: undefined;
    mealOption: "";
    mealOptionSecond: "";
    mealOptionPlusOne: "";
    dietaryReqs: "";
    message: "";
}